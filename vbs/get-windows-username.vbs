' Description : vbs script which display only the username from the pc in a msgBox (popup)
' Author : Thibaud Robin

Set wshShell = CreateObject("WScript.Shell")
Dim username
username = wshShell.ExpandEnvironmentStrings("%USERNAME%")
msgBox "Nom d'utilisateur : " + username, vbInformation, "Computer information"
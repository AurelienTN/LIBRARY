' Description : vbs script which display all great informations from the pc in a msgBox (popup)
' Author : Thibaud Robin

Set wshShell = CreateObject("WScript.Shell")

COMPUTERNAME = wshShell.ExpandEnvironmentStrings("%COMPUTERNAME%")
USERNAME = wshShell.ExpandEnvironmentStrings("%USERNAME%")
MASTER = wshShell.ExpandEnvironmentStrings("%MASTER%")
NUMBER_OF_PROCESSORS = wshShell.ExpandEnvironmentStrings("%NUMBER_OF_PROCESSORS%")
OS = wshShell.ExpandEnvironmentStrings("%OS%")
PROCESSOR_ARCHITECTURE = wshShell.ExpandEnvironmentStrings("%PROCESSOR_ARCHITECTURE%")
PROCESSOR_IDENTIFIER = wshShell.ExpandEnvironmentStrings("%PROCESSOR_IDENTIFIER%")
USERDOMAIN = wshShell.ExpandEnvironmentStrings("%USERDOMAIN%")

msgBox "COMPUTERNAME : " + COMPUTERNAME  + Chr(13) + "USERNAME : " + USERNAME + Chr(13) + "MASTER : " + MASTER + Chr(13) + "NUMBER_OF_PROCESSORS : " + NUMBER_OF_PROCESSORS  + Chr(13) + "OS : " + OS+ Chr(13) + "PROCESSOR_ARCHITECTURE : " + PROCESSOR_ARCHITECTURE + Chr(13) + "PROCESSOR_IDENTIFIER : " + PROCESSOR_IDENTIFIER  + Chr(13) + "USERDOMAIN : " + USERDOMAIN, vbInformation, "Computer information"
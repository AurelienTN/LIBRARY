#!/usr/bin/python
# Description : color file with personalizable print
# Author : Thibaud Robin

class Color:
	PURPLE = '\033[95m'
	CYAN = '\033[96m'
	DARKCYAN = '\033[36m'
	BLUE = '\033[94m'
	GREEN = '\033[92m'
	YELLOW = '\033[93m'
	RED = '\033[91m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'
	END = '\033[0m'

SUCCESS = Color.GREEN + Color.BOLD + '[+] ' + Color.END
INFO = Color.BLUE + Color.BOLD + '[*] ' + Color.END
QUESTION = Color.CYAN + Color.BOLD + '[?] ' + Color.END
WARNING = Color.YELLOW + Color.BOLD + '[!] ' + Color.END
DANGER = Color.RED + Color.BOLD + '[x] ' + Color.END

def number_choice(number):
	return "[" + Color.RED + Color.BOLD + str(number) + Color.END + "]"
# Définition d'un client réseau gérant en parallèle l'émission
# et la réception des messages (utilisation de 2 THREADS).

HOST = '127.0.0.1'
PORT = 40017
 
import socket, os
execfile("lib/menu.py")
os.system("clear")

class Client():

    def __init__(self, co):
        self.connection = co
        
    def run(self):
        stop = False
        while stop == False:
            message_to_send = input(">>> ")
            
            if message_to_send == "exit" :
                stop = True
                self.connection.send(message_to_send.encode())
            else:
                self.connection.send(message_to_send.encode())
                message_received = self.connection.recv(128)
                print(message_received.decode() + "\n")


connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    connection.connect((HOST, PORT))
except socket.error:
    print ("[x] Connection failed...")

print ("[+] Connection established !")

menu = Menu()
menu.print_menu()
client = Client(connection)
# client.run()

connection.close()
print("[x] Connection closed !")


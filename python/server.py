# Utilise les threads pour gérer les connexions clientes en parallèle.

HOST = '127.0.0.1'
PORT = 40017
 
import socket, sys, threading, os

os.system('clear')

class Server(threading.Thread):

    def __init__(self, conn):
        threading.Thread.__init__(self)
        self.connexion = conn
        
    def run(self):
        current_client_name = self.getName()        # Chaque thread possède un current_client_name

        stop = False
        while stop == False:
            message_received = self.connexion.recv(1024)
            if message_received == b"exit":
                stop = True

            print("\nRECEIVED [%s] %s" % (current_client_name, message_received.decode()))

            # Send the message only to the sender
            for client_name in connected_clients:
                if client_name == current_client_name:
                    message_sended = "{'status':'ok'}"
                    connected_clients[client_name].send(message_sended.encode())
                    print("SENDED [%s] %s\n" % (current_client_name, message_sended))

        self.connexion.close()                      # Close server connexion
        del connected_clients[current_client_name]  # Delete current user

        print("\n[*] Client %s disconnected" % current_client_name)


# Server init
co = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    co.bind((HOST, PORT))
except socket.error:
    print("[x] Error : impossible to create connection on port " + str(PORT))
    sys.exit()


print("[*] Server is listening on port " + str(PORT))
co.listen(5)

connected_clients = {}

while True:    
    connexion, adresse = co.accept()

    server = Server(connexion)
    server.start()

    client_name = server.getName()
    connected_clients[client_name] = connexion
    print("[+] Client %s connecte : %s:%s." % (client_name, adresse[0], adresse[1])) 

    

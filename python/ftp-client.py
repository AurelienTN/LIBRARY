#!/usr/bin/python3
# Description : Ftp client class

import os, sys
from ftplib import FTP

class FtpClient():

    def __init__(self, hostname, port):
        self.hostname = hostname
        self.port = port
        self.ftp_co = FTP('')
        self.username = ""
        self.password = ""


    def connection(self, username, password):
        try:
            self.username = username
            self.password = password
            self.ftp_co.connect(self.hostname, self.port)
            self.ftp_co.login(self.username, self.password)
            print("[+] Successfull connection to FTP server " + self.hostname + ":" + str(self.port) + "\n")

        except Exception as e:
            print("[x] Error : connection failed - server unreachable...")
            sys.exit()


    def deconnection(self):
        self.ftp_co.quit()
        print("\n" + "[+] Successfull deconnection to FTP server " + self.hostname + ":" + str(self.port))


    def upload(self, filename):
        try:
            self.ftp_co.storbinary('STOR '+ filename, open(filename, 'rb'))
            print("[+] Successfull upload of " + filename)

        except Exception as e:
            print("[x] Error : upload failed...")


    def download(self, filename):
        try:
            localfile = open(filename, 'wb')
            self.ftp_co.retrbinary('RETR ' + filename, localfile.write, 1024)
            localfile.close()
            print("[+] Successfull download of " + filename)
            
        except Exception as e:
            print("[x] Error : download failed...")


    def delete(self, filename):
        try:
            self.ftp_co.delete(filename)
            print("[+] Successfull delete of " + filename)

        except Exception as e:
            print("[x] Error : upload failed...")


if __name__ == '__main__':

    client = FtpClient("127.0.0.1", 1026)

    # Connection
    client.connection("superadmin", "superadmin")

    # Upload a document
    # client.upload("test.txt")

    # Download a document
    # client.download("test.txt")
    # client.delete("test.txt")

    # Deconnection
    client.deconnection()
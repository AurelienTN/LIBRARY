#!/usr/bin/python3
# Author : Thibaud Robin
# Description : authenticate yourself in facebook with python

import requests
from bs4 import BeautifulSoup

EMAIL =  u'your_facebook_mail'
PASSWORD = u'your_facebook_password'

session = requests.Session()

# Facebook authentication
auth_page = session.get("https://www.facebook.com/")
auth_page_soup = BeautifulSoup(auth_page.text, 'html.parser')
auth_form = auth_page_soup.find('form')
auth_fields = auth_form.findAll('input')
formdata = dict((field.get('name'), field.get('value')) for field in auth_fields)

formdata['email'] = EMAIL
formdata['pass'] = PASSWORD

confirm_page = session.post(auth_form['action'], data=formdata)
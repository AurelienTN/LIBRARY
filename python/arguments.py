#!/usr/bin/python
# Description : arguments and parameter in Python
# Author : Thibaud Robin

# Execute file with arguments
# TODO

# Function with optional arguments
def myfunc(*args, **keyword):
	print("args : ", args)
	print("keyword : ", keyword)

myfunc("hello world !", "what's up ?")
# DISPLAY :
# ('args : ', ('hello world !', "what's up ?"))
# ('keyword : ', {})

myfunc("hello world !")
# DISPLAY
# ('args : ', ('hello world !',))
# ('keyword : ', {})

myfunc()
# DISPLAY
# ('args : ', ())
# ('keyword : ', {})

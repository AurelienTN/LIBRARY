#!/usr/bin/python3
# Description : send an mail in python with gmail smtp server.
# Command : python3 mail -l <smtp_login> -p <smtp_password> -r <recipient_mail> -s <subject> -m <message>
# Author : Thibaud Robin

import smtplib, platform, sys, argparse
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


# Parse arguments
parser = argparse.ArgumentParser(description='Simplify your mail sending')
parser.add_argument('--login', '-l', action='store', dest='login', help='your smtp login')
parser.add_argument('--password', '-p', action='store', dest='password', help='your smtp password')
parser.add_argument('--recipient', '-r', action='store', dest='recipient', help='the recipient mail adress')
parser.add_argument('--subject', '-s', action='store', dest='subject', help='your mail subject')
parser.add_argument('--message', '-m', action='store', dest='message', help='your mail message')
parser.add_argument('--version', '-v', action='version', version='%(prog)s 1.0')
args, unknown = parser.parse_known_args()


class MailLibrary(object):

	def __init__(self):
		self.smtp_login = ""
		self.smtp_password = ""
		self.recipient = ""
		self.subject = ""
		self.message = ""


	def launched_in_cmd(self):
		if args.login != None or args.password != None or args.recipient != None or args.subject != None or args.message != None:
			return True
		else:
			return False


	def check_arguments(self):
		if args.login != None and args.password != None and args.recipient != None and args.subject != None and args.message != None:
			self.smtp_login = args.login
			self.smtp_password = args.password
			self.recipient = args.recipient
			self.subject = args.subject
			self.message = args.message
			return True

		elif self.smtp_login != "" and self.smtp_password != "" and self.recipient != "" and self.subject != "" and self.message != "":
			return True

		else:
			return False


	def set_smtp_credentials(self, smtp_login, smtp_password):
		self.smtp_login = smtp_login
		self.smtp_password = smtp_password


	def set_recipient(self, recipient):
		self.recipient = recipient


	def set_subject(self, subject):
		self.subject = subject


	def set_message(self, message):
		self.message = message


	def set_all_informations(self, recipient, subject, message):
		self.recipient = recipient
		self.subject = subject
		self.message = message


	def send_mail(self):
		try:
			if self.check_arguments() == False:
				raise Exception("Some data are empty. If you are in command line check your syntax :")
			else:
				msg = MIMEMultipart()
				
				msg['From'] = platform.node()
				msg['To'] = self.recipient
				msg['Subject'] = self.subject 

				msg.attach(MIMEText(self.message, 'html'))

				mailserver = smtplib.SMTP('smtp.gmail.com', 587)
				mailserver.ehlo()
				mailserver.starttls()
				mailserver.ehlo()
				mailserver.login(self.smtp_login, self.smtp_password)
				mailserver.sendmail(self.smtp_login, self.recipient, msg.as_string())
				mailserver.quit()

				print('[+] SUCCESS : Mail sended !')

		except Exception as e:
			print('[x] ERROR : ' + str(e) + '\n')
			parser.print_help()



# Send email via cmd line
# =============================

mail = MailLibrary()
if mail.launched_in_cmd():
	mail.send_mail()


# Include and call the class
# =============================

# from mail import MailLibrary
# mail = MailLibrary()
# mail.set_smtp_credentials(SMTP_EMAIL, SMTP_PASSWORD)
# mail.set_all_informations(RECIPIENT, "your subject", "your message")
# mail.send_mail()
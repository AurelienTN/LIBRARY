#!/usr/bin/python
# Description : generate a strong password
# Author : Thibaud Robin

import string, random

PASSWORD_LENGTH = 20
password = []

for i in range(0, PASSWORD_LENGTH):
	password.append(random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits + string.punctuation))

random.shuffle(password)
print(''.join(password))

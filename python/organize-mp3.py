# Description : python script to organize some get and edit informations from music
# Author : Thibaud Robin
# !/usr/bin/python3

from eyed3 import id3
import eyed3, os

PATH = "/home/path/to/your/music.mp3"
ELEMENT_TO_DELETE = "[www.website.com]"


def has_numbers(inputString):
	return any(char.isdigit() for char in inputString)


def remove_element_in_filename(filename, element_to_delete):	# Delete if ELEMENT_TO_DELETE it presents in the title music
	if element_to_delete in filename:
		new_filename = filename.replace(" " + element_to_delete, "")
		os.rename(PATH + filename, PATH + new_filename)
		filename = new_filename

	return filename


def remove_numbers_behind_filename(filename):	# If the music is like "01 - My title", delete numbers -> "My title"
	if has_numbers(filename[:2]):
		new_filename = filename[5:]
		os.rename(PATH + filename, PATH + new_filename)
		filename = new_filename

	return filename


def remove_element_in_title_tag(element_to_delete): 	# Delete if ELEMENT_TO_DELETE it presents in the title tag music
	global read_track, write_track
	if read_track.title is not None:
		if element_to_delete in read_track.title:
			write_track.tag.title = read_track.title.replace(element_to_delete, "")
			write_track.tag.save()


def remove_album_tag():		# Delete the album tag of a track
	global write_track
	if write_track.tag is not None:
		write_track.tag.album = u""
		write_track.tag.save()


def remove_images_and_tag_composer(filename):
	os.system("eyeD3 '" + PATH + filename + "' --remove-all-images --composer ''")


for filename in os.listdir(PATH):

	if '.DS_Store' not in filename:

		filename = remove_element_in_filename(filename, ELEMENT_TO_DELETE)
		filename = remove_numbers_behind_filename(filename)

		read_track = id3.Tag()
		read_track.parse(PATH + filename)

		write_track = eyed3.load(PATH + filename)

		remove_element_in_title_tag(ELEMENT_TO_DELETE)
		remove_album_tag()
		remove_images_and_tag_composer(filename)

		print("New filename : " + filename)
		print("New artist tag : " + str(read_track.artist))
		print("New album tag : " + str(read_track.album))
		print("New title tag : " + str(read_track.title))
		print("\n")
#!/usr/bin/python
# Description : generate hash in python
# Author : Thibaud Robin

def string_to_bin(string):
	binarized = ""
	for char in string:
		tmp = bin(ord(char))[2:]
		tmp = '%08d' %int(tmp)
		binarized += tmp
		
	return binarized

def hashkey_to_hex(key):
	hashkey = hashlib.sha256(key)
	return hashkey.hexdigest()

def hashkey_to_digest(key):
	hashkey = hashlib.sha256(key)
	return hashkey.digest()

print hashkey_to_hex("password1234")
print hashkey_to_digest("password1234")

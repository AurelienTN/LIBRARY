#!/usr/bin/python3
# Description : Ftp server class
# Doc : http://pyftpdlib.readthedocs.io/en/latest/api.html

import os, sys
from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer

class FtpServer():

    def __init__(self, hostname, port):
        self.hostname = hostname
        self.port = port
        self.authorizer = DummyAuthorizer()
    

    def add_user(self, username, password):
        user_path = "/tmp/ftpusers/" + username
        os.system("mkdir -p /tmp/ftpusers/" + username)
        self.authorizer.add_user(username, password, user_path, perm="elradfmwMT", msg_login="Welcome " + username + " ! :)", msg_quit="Goodbye " + username)


    def listener(self):
        handler = FTPHandler
        handler.authorizer = self.authorizer
        handler.max_login_attempts = 5
        server = FTPServer((self.hostname, self.port), handler)
        server.serve_forever()


if __name__ == '__main__':
    server = FtpServer("127.0.0.1", 1026)

    # Récupérer les identifiants de la base de donnée serait plus judicieux    
    server.add_user("superadmin", "superadmin")
    server.add_user("admin_lyon", "admin_lyon")
    server.add_user("admin_nantes", "admin_nantes")
    server.add_user("admin_strasbourg", "admin_strasbourg")

    server.listener()

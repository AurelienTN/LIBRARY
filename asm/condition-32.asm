; Description : condition in assembly 32 bits
; Author : Thibaud Robin

; TO COMPILE :
; ---------------
; 	|  nasm -f elf condition-32.asm && ld -o condition-32 condition-32.o -melf_i386 && ./condition-32

; Pseudo-code
; ------------------------
; 	|  if(eax == 5) {
;	|	ebx = 4;
;	|	printf('True !');
; 	|  } else {
;	|      	ebx = 8;
;	|	printf('False...');
; 	|  }


WRITE equ 4
STDOUT equ 1


section .data
	trueMsg: db 'True !', 10
	trueMsgSize: equ $-trueMsg
	falseMsg: db 'False...', 10
	falseMsgSize: equ $-falseMsg


section .text
	global _start


_start:
	mov eax, 5				; True
	;mov eax, 4				; False

	cmp eax, 5				; if eax == 5...
	jz then					; ...jump to 'then'...

	mov ebx, 8				; else ebx = 8 and continue to next
	call _writeFalse
	jmp next

then:
	mov ebx, 4
	call _writeTrue

next:
	mov eax, 1				; System call 'exit'
	xor ebx, ebx				; Return code
	int 80h


_writeTrue:
	mov eax, WRITE				; System call "write" (sys_write)
	mov ebx, STDOUT				; File descriptor
	mov ecx, trueMsg 			; String address
	mov edx, trueMsgSize			; Size string
	int 80h					; System call
	ret


_writeFalse:
	mov eax, WRITE				; System call "write" (sys_write)
	mov ebx, STDOUT				; File descriptor
	mov ecx, falseMsg 			; String address
	mov edx, falseMsgSize			; Size string
	int 80h					; System call
	ret
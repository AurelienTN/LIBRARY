; Description : write something in assembly 32 bits
; Author : Thibaud Robin

; TO COMPILE :
; ---------------
; 	|  nasm -f elf write-32.asm && ld -o write-32 write-32.o -melf_i386 && ./write-32

WRITE equ 4
STDOUT equ 1

section .data
	msg: db 'Hello world !',10	; 10 -> remove the \n (%)
	msgSize: equ $-msg

section .text
	global _start

_start:
	call _write

	mov eax, 1		; System call 'exit'
	xor ebx, ebx		; Return code
	int 80h

_write:
	mov eax, WRITE		; System call "write" (sys_write)
	mov ebx, STDOUT		; File descriptor
	mov ecx, msg 		; String address ONLY STRING POINTER NO REGISTRY OR INT
	mov edx, msgSize	; Size string
	int 80h			; System call
	ret
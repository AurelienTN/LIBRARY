; Description : while in assembly 32 bits
; Author : Thibaud Robin

; TO COMPILE :
; ---------------
; 	|  nasm -f elf while-32.asm && ld -o while-32 while-32.o -melf_i386 && ./while-32

; Pseudo-code
; ------------------------
; 	|  while(esi != 0) {
;	|	esi = esi -1;
;	|	printf('%d', esi);
; 	|  }


WRITE equ 4
STDOUT equ 1


section .text
	global _start


_start:
	mov esi, 9
	jmp while

while:
	cmp esi, 0			; Check if esi == 0
	je endWhile			; If yes, jump to 'endwhile'
	call _writeEsi			; Print esi
	sub esi, 1			; Else, esi = esi - 1
	jmp while 			; Return to the start of the loop

endWhile:
	mov eax, 1			; System call 'exit'
	xor ebx, ebx			; Return code
	int 80h


_writeEsi:
	add esi, 0x30			; Convert to string
	push esi 			; Push to the stack

	mov eax, WRITE			; System call "write" (sys_write)
	mov ebx, STDOUT			; File descriptor
	mov ecx, esp 			; Pointer on the first address on the stack
	mov edx, 1			; Size string
	int 80h				; System call

	pop esi 			; Remove string od the stack
	sub esi, 0x30 			; Reconvert to int
	ret
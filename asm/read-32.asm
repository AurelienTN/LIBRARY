; Description : read something in assembly 32 bits
; Author : Thibaud Robin

; TO COMPILE :
; ---------------
; 	|  nasm -f elf read-32.asm && ld -o read-32 read-32.o -melf_i386 && ./read-32

READ equ 3
STDIN equ 2

section .bss
	msg resw 4		; Reserve 4 bytes in memory

section .text
	global _start

_start:
	call _read		; Launch read function

	mov ecx, [msg]		; Move msg read. Ready to print ! 

	mov eax, 1		; System call 'exit'
	xor ebx, ebx		; Return code
	int 80h

_read:
	mov eax, READ		; System call "read" (sys_read)
	mov ebx, STDIN		; File descriptor
	mov ecx, msg 		; String address
	mov edx, 4		; Size string
	int 80h			; System call
	ret
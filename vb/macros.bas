' Description : vb script to display a popup in the opening of a document
' Author : Thibaud Robin

Sub AutoOpen()
	message = "Hello," +  Chr(13)  +  Chr(13)  + "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam gravida tempus nibh, et porta velit pellentesque vitae. Fusce in elementum risus. Donec arcu risus, commodo quis ultrices ac, fringilla in tellus. Praesent eget mauris at mauris tincidunt finibus. Maecenas nibh enim, viverra ac faucibus vitae, feugiat vitae diam. Maecenas ultrices ex et nunc blandit sollicitudin sodales non eros. Vestibulum semper ex in justo congue vestibulum." +  Chr(13)  +  Chr(13)  + "Thibaud Robin"

	msgBox message, vbInformation, "New message"
End Sub


# Description : display a popup in powershell
# Source : cnf1g.com/?p=279
# Usage example : ./Show-Message -Message "Body message" -Title "Title message" -YesNo -IconQuestion

param (
    [string]$Message = ":)",
    [string]$Title = ":)",
    [switch]$OKCancel,
    [switch]$AbortRetryIgnore,
    [switch]$YesNoCancel,
    [switch]$YesNo,
    [switch]$RetryCancel,
    [switch]$IconErreur,
    [switch]$IconQuestion,
    [switch]$IconAvertissement,
    [switch]$IconInformation
)


if ($OKCancel) { $Btn = 1 }
elseif ($AbortRetryIgnore) { $Btn = 2 }
elseif ($YesNoCancel) { $Btn = 3 }
elseif ($YesNo) { $Btn = 4 }
elseif ($RetryCancel) { $Btn = 5 }
else { $Btn = 0 }


if ($IconErreur) {$Icon = 16 }
elseif ($IconQuestion) {$Icon = 32 }
elseif ($IconAvertissement) {$Icon = 48 }
elseif ($IconInformation) {$Icon = 64 }
else {$Icon = 0 }


[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null
$response = [System.Windows.Forms.MessageBox]::Show($Message, $Title , $Btn, $Icon)
echo $response
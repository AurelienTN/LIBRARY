# Description : send mail in powershell
# Author : Thibaud Robin
# Usage : send-mail.ps1 -From <mail-from> -To <mail-to> -Subject <subject> -Body <body>

param(
    [Parameter(Mandatory=$false)][string] $from = "from@xxx.fr",
    [Parameter(Mandatory=$false)][string] $to = "to@xxx.fr",
    [Parameter(Mandatory=$false)][string] $subject = "Empty subject",
    [Parameter(Mandatory=$false)][string] $body = "Empty message"
)

Send-MailMessage -From $from -To $to -Subject $subject -SmtpServer "your-smtp-server" -BodyAsHtml $body -Encoding UTF8
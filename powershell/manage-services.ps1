﻿# Description : manage services on a computer
# Author : Thibaud Robin
# Usage : manage-services.ps1 --start <computer_name> <service_name>  -->  Start service_name
#         manage-services.ps1 --stop <computer_name> <service_name>   -->  Stop service_name
#         manage-services.ps1 --show-al <computer_name>               -->  Show all service from a pc
#         manage-services.ps1 --show <computer_name> <service_name>   -->  Show the service

# Ex : service.ps1 start RemoteRegistry U27154

param(
    [Parameter(Mandatory=$True)]
    [string] $action,

    [Parameter(Mandatory=$True)]
    [string] $computer_name,

    [string] $service_name
)

#echo "action = $action, service_name = $service_name, computer_name = $computer_name"


function manage_services {

    if($action -ne "" -and $computer_name -ne "") {

        if(pc_available -computer $computer_name) {

            if($service_name -ne "") {

                # Find the service and the computer
                $service = gwmi -Computer $computer_name win32_service -Filter "name='$($service_name)'"

                if($action -eq "--start") {
                    $result = $service.StartService()
                    Write-Host "[+] $service have been started..." -foreground "cyan"
                }

                elseif($action -eq "--stop") {
                    $result = $service.StopService()
                    Write-Host "[+] $service have been stopped !" -foreground "cyan"
                }

                elseif($action -eq "--show") {
                    echo $service
                }

                else {
                    Write-Host "[x] Syntax error. Action $action unavailable" -foreground "red"
                }

            } else {
                gwmi -Computer $computer_name win32_service | select Name, StartMode, State, ProcessID
            }

        } else {
            Write-Host "[x] Error : pc $computer_name is unreachable..." -foreground "red"
        }

    } else {
            Write-Host "[x] Syntax error. Commands available :" -foreground "red"
            Write-Host "    manage-services.ps1 --start <computer_name> <service_name> --> Start service_name" -foreground "red"
            Write-Host "    manage-services.ps1 --stop <computer_name> <service_name>  --> Stop service_name" -foreground "red"
            Write-Host "    manage-services.ps1 --show-al <computer_name>              --> Show all service from a pc" -foreground "red"
            Write-Host "    manage-services.ps1 --show <computer_name> <service_name>  --> Show the service" -foreground "red"
    }
}


function pc_available {
    param($computer)

    if(Test-Connection $computer_name -Count 1 -Quiet) {
        return $true
    } else {
        return $false
    }
}

manage_services
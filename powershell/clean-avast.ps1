# Description : delete avast exe and service on a computer
# Author : Thibaud Robin
# Usage : clean-avast.ps1 computer_name

param(
    [string]$computer_name
)

$tab_proc = @('avastUI.exe', 'AvastSvc.exe', 'afwServ.exe')
$tab_service = @('Avast')

if($computer_name -eq "") {
    Write-Host "[?] Computer name : " -foreground "cyan" -NoNewline
    $computer_name = Read-Host
}

Write-Host "[*] Connection to $computer_name..." -foreground "gray"

if(Test-Connection $computer_name -Count 1 -Quiet) {
    Write-Host "[+] $computer_name is up !" -foreground "green"
    

    # Kill all process
    Write-Host "[*] Start of processes shutdown..." -foreground "gray"

    foreach($proc in $tab_proc) {
        $ok = $true

        try {
            (gwmi -computername $computer_name win32_process -Filter "name = '$($proc)'").Terminate()

        } catch {
            Write-Host "[x] Error : impossible to kill $($proc) - not found on $computer_name..." -foreground "red"
            $ok = $false
        }
          
        if($ok) {
            Write-Host "[+] Process $($proc) have been killed !" -foreground "green"
        }
    }



    # Kill all services
    Write-Host "[*] Start of services shutdown..." -foreground "gray"

    foreach($service in $tab_service) {
        $ok = $true

        try {
            (gwmi -computername $computer_name win32_service -Filter "name = '$($service)'").delete()

        } catch {
            Write-Host "[x] Error : impossible to delete service $($service) - not found on $computer_name..." -foreground "red"
            $ok = $false
        }

        if($ok) {
            Write-Host "[+] Service $($service) have been killed !" -foreground "green"
        }
    }


    # Delete the folder with exe
    $query = Get-WMIObject -ComputerName $computer_name -Query "Select * From Win32_Directory Where name='C:\\Program Files\\Avast'"
    $query | Remove-WMIObject


    # Delete key in regedit
    # TODO

} else {
    Write-Host "[x] Error : $computer_name is not connected !" -foreground "red"
}


# Description : export all users who can read a specific folder (= export all users from a specific AdGroup)
# Author : Thibaud Robin

<#
    Utilisation
    =================
    ./export-folders-members.ps1 <chemin_vers_le/les_dossiers> <nom_du/des_dossiers> <chemin_vers_le_fichier_export_csv>
    
    Exemples
    ============                                                                                                       
        Obtenir tous les utilisateurs ayant acc�s au repertoire C:\dir
        --------------------------------------------------------------
        ./export-folders-members.ps1 'C:' 'dir' 'C:\export-members.csv'
                                                                                                        
        Obtenir tous les utilisateurs ayant acc�s aux repertoires C:\dir1, C:\dir2 et C:\dir3
        -------------------------------------------------------------------------------------
        ./export-folders-members.ps1 'C:' 'dir1','dir2','dir3' 'C:\export-members.csv'
#>

param(
    [Parameter(Mandatory=$false)][string]$PATH_TO_ALL_FOLDERS,
    [Parameter(Mandatory=$false)][string[]]$FOLDERS,
    [Parameter(Mandatory=$false)][string]$CSV_FILE
)

if($PATH_TO_ALL_FOLDERS -eq "" -or $FOLDERS -eq "" -or $CSV_FILE -eq "") {
    write-host " [x] ERREUR : Arguments incorrects !`r`n" -ForegroundColor Red
    write-host " [*] USAGE : ./export-folders-members.ps1 <chemin_vers_le/les_dossiers> <nom_du/des_dossiers> <chemin_vers_le_fichier_export_csv>" -ForegroundColor Cyan
    write-host "  |                                                                                                                              " -ForegroundColor Cyan
    write-host "  |      | Obtenir tous les utilisateurs ayant acc�s au repertoire C:\dir                                                        " -ForegroundColor Cyan
    write-host "  +--[*] | ==========================================================================                                            " -ForegroundColor Cyan
    write-host "  |      | ./export-folders-members.ps1 'C:' 'dir' 'C:\export-members.csv'                                                       " -ForegroundColor Cyan
    write-host "  |                                                                                                                              " -ForegroundColor Cyan
    write-host "  |      | Obtenir tous les utilisateurs ayant acc�s aux repertoires C:\dir1, C:\dir2 et C:\dir3                                 " -ForegroundColor Cyan
    write-host "  +--[*] | =======================================================================================                               " -ForegroundColor Cyan
    write-host "  |      | ./export-folders-members.ps1 'C:' 'dir1','dir2','dir3' 'C:\export-members.csv'                                        " -ForegroundColor Cyan
    write-host "  |                                                                                                                              " -ForegroundColor Cyan

} else {

    try {
        Write-Host "`r`n [*] D�marrage du script..." -ForegroundColor Cyan

        $export = @()

        foreach($folder in $FOLDERS) {

            Write-Host " [*] Export des utilisateurs du r�pertoire : $PATH_TO_ALL_FOLDERS\$folder`r`n" -ForegroundColor Cyan

            $groups_folder = Get-ACL "$PATH_TO_ALL_FOLDERS\$folder" | %{$_.Access} | sort-object -unique IdentityReference | select IdentityReference
        
            if($groups_folder -ne $null) {
                Write-Host " [*] Groupe(s) ayant acc�s au r�pertoire" -ForegroundColor Cyan
                echo $groups_folder
                echo "`r`n"
            }

            foreach($group_folder in $groups_folder) {

                if($group_folder -like "*SODIAAL*") {

                    $group_folder = $group_folder.IdentityReference.ToString().Substring(8)

                    try {
                        $group_members = Get-ADGroupMember -Identity $group_folder -Recursive | select Name
                        Write-Host " [*] Groupe $group_folder : $($group_members.Length) personne(s)" -ForegroundColor Cyan
                        Write-Host "  |" -ForegroundColor Cyan

                        foreach($member in $group_members) {

                            if($member -ne $null) {
                                Write-Host "  |--[*] Utilisateur : $member" -ForegroundColor Cyan
                                $export += "$($member.Name);$PATH_TO_ALL_FOLDERS\$folder"
                            }
                        }
                
                        Write-Host "  |" -ForegroundColor Cyan

                    } catch {
                        # Write-Host " [x] Erreur : $($_.Exception.Message)`r`n" -ForegroundColor Red
                    }

                }
            }
        }

        if($export.Length -ne 0) {
            "Nom;Repertoire" > $CSV_FILE
            echo $export >> $CSV_FILE
            Write-Host " [+] Rapport g�n�r� dans $CSV_FILE`r`n" -ForegroundColor Green

        } else {
            Write-Host " [!] Aucun utilisateur pour le r�pertoire : $PATH_TO_ALL_FOLDERS\$folder`r`n" -ForegroundColor Yellow
        }

    } catch {
        Write-Host " [x] Erreur : $($_.Exception.Message)`r`n" -ForegroundColor Red
    }
}
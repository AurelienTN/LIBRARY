﻿# Description : manage regedit on a computer
# Author : Thibaud Robin
# Usage : manage-regedit.ps1                          -->  Show regedit from script data 
#         manage-regedit.ps1 <@ip_computer>           -->  Show regedit from <@ip_computer> computer
#         manage-regedit.ps1 <@ip_computer> delete    -->  Show regedit from <@ip_computer> computer and delete a key/value

# Ex : manage-regedit.ps1 u26000


param([string] $computer, [string] $choice)

#------------------------------- DATA TO MODIFY ---------------s---------------------+
# Example :                                                                         |
#  - $tab_computer_name = @('u27154')                                               |
#  - $tab_computer_name = @('192.168.67.8')                                         |
#  - $tab_computer_name = @('u27154', 'u56246', '10.35.67.78')                      |
                                                                                    #
$tab_computer_name = @('u27154')                                                    #
$tab_key_path = @("SOFTWARE\Microsoft\Windows\CurrentVersion\Run",                  #
                  "SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce",              #
                  "SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Run",      #
                  "SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\RunOnce")  #
# ----------------------------------------------------------------------------------+


#
#  _   _   _   _   _   _   _   _
# /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\
# /!\ DON'T MODIFY CODE BELOW /!\
# /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\
#
#


##########################################################################################
#################################### GLOBAL FUNCTIONS ####################################
##########################################################################################

function start_remote_registry {
    param($computer)
    .\manage-services.ps1 --start $computer RemoteRegistry
}


function stop_remote_registry {
    param($computer)
    .\manage-services.ps1 --stop $computer RemoteRegistry
}


function pc_available {
    param($computer)

    if(Test-Connection $computer -Count 1 -Quiet) {
        return $true
    } else {
        return $false
    }
}


function get_hkusers_repertory{
    param($computer)

    $tab_repertory = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey('Users', $computer).GetSubKeyNames()
    $tab_to_return = @()

    foreach($repertory in $tab_repertory){
        if($repertory -notmatch '_Classes' -and $repertory -match 'S-1-5-21') {
            $tab_to_return += $repertory
        }
    }

    return ,$tab_to_return
}


function echo_key_and_value{
    param($computer, $hkey, $key_path)

    $value = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey($hkey, $computer).OpenSubKey($key_path)

    if($value -ne $null -and $value -ne "") {
               
        Write-Host "    [+]" -foreground "Cyan" -NoNewline
        Write-Host " $($value)" -foreground "Cyan"
        Write-Host "     |" -foreground "Cyan"

        if($value.GetValueNames() -ne $null -and $value.GetValueNames() -ne "") {
            foreach($val in $value.GetValueNames()) {
                Write-Host "     |--- " -foreground "Cyan" -nonewline
                Write-Host "$($val)" -nonewline
                Write-Host " > " -foreground "Cyan" -nonewline
                Write-Host "$($value.GetValue($val))"
                Write-Host "     |" -foreground "Cyan"
            }

        } else {
            Write-Host "     |--- " -foreground "Cyan" -NoNewline
            Write-Host "No key found..." -foreground "Yellow"
            Write-Host "     |" -foreground "Cyan"
        }
    }
}


##########################################################################################
################################# MAIN FUNCTIONS #########################################
##########################################################################################

function get_regedit {
    param($tab_computer, $tab_key_path)

    $tab_hkey = @('LocalMachine', 'Users')

    foreach($computer in $tab_computer) {

        if(pc_available -computer $computer) {
            Write-Host " |" -foreground "Cyan"
            Write-Host " |  ╔═╗╔═╗╔╦╗╔═╗╦ ╦╔╦╗╔═╗╦═╗" -ForegroundColor Magenta
            Write-Host "[+] ║  ║ ║║║║╠═╝║ ║ ║ ║╣ ╠╦╝" -ForegroundColor Magenta
            Write-Host " |  ╚═╝╚═╝╩ ╩╩  ╚═╝ ╩ ╚═╝╩╚═" -ForegroundColor Magenta
            Write-Host " |           $computer " -ForegroundColor Magenta
            Write-Host " |" -foreground "Cyan"
            Write-Host " |" -foreground "Cyan"
            Write-Host " |" -foreground "Cyan"
            start_remote_registry $computer
            Write-Host " |" -foreground "Cyan"
            Write-Host " +---+" -foreground "Cyan"
        
            $tab_user_repertory = get_hkusers_repertory -computer $computer

            foreach($hkey in $tab_hkey) {
            
                foreach($key_path in $tab_key_path) {

                    if($hkey -eq 'Users' -and $key_path.ToString().Contains("Wow6432Node") -eq $False) {
                        
                        $key_path_original = $key_path

                        foreach($user_repertory in $tab_user_repertory) {
                            
                            $key_path = "$($user_repertory)\" + $key_path_original

                            echo_key_and_value $computer $hkey $key_path
                        }

                    } elseif($hkey -eq 'LocalMachine'){
                        echo_key_and_value $computer $hkey $key_path
                    }
                }
            }
            
            Write-Host " +---+" -foreground "Cyan"
            Write-Host " |" -foreground "Cyan"
            stop_remote_registry $computer
            Write-Host " |" -foreground "Cyan"
            Write-Host " |" -foreground "Cyan"

        } else {
            Write-Host "[x] CONNEXION ERROR : PC $computer is unreachable, probably offline..." -foreground "red"
        }
    }
}


function delete_regedit {
    param($computer, $key_path, $key_value)
   
    while($computer -eq $null) {
        Write-Host " | " -foreground "Yellow"
        Write-Host "[?] What is the computer (ex: u27000) : " -foreground "Yellow" -NoNewline
        $computer = Read-Host
        Write-Host " | " -foreground "Yellow"
    }

    while($key_path -eq $null) {
        Write-Host " | " -foreground "Yellow"
        Write-Host "[?] What is the keypath (ex: HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run) : " -foreground "Yellow" -NoNewline
        $key_path = Read-Host
        Write-Host " | " -foreground "Yellow"

        #Determine if $hkey = LocalMachine or Users
        if($key_path.Contains("HKEY_LOCAL_MACHINE")) {
            $hkey = "LocalMachine"
            $key_path = $key_path.ToString().Replace("HKEY_LOCAL_MACHINE\", "")   # we can only open software\... without HK---

        } else {
            $hkey = "Users"
            $key_path = $key_path.ToString().Replace("HKEY_USERS\", "")           # we can only open software\... without HK---
        }

    }

    while($key_value -eq $null) {
        Write-Host " | " -foreground "Yellow"
        Write-Host "[?] What is the value (ex: IgfxTray) : " -foreground "Yellow" -NoNewline
        $key_value = Read-Host
        Write-Host " | " -foreground "Yellow"
    }

    $value = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey($hkey, $computer).OpenSubKey($key_path, $True)

    if($value -ne $null -and $value -ne "" -and $value.GetValueNames() -ne $null -and $value.GetValueNames() -ne "" -and $value.GetValueNames().Contains($key_value)) {
        Write-Host " | " -foreground "Yellow"
        Write-Host "[?] Are you sure to delete $key_value (y/n) : " -foreground "Yellow" -NoNewline
        $ok_to_delete = Read-Host
        Write-Host " | " -foreground "Yellow"

    } else {
        Write-Host " | " -foreground "Red"
        Write-Host "[x] Key or value are incorrect..." -foreground "Red"
        Write-Host " | " -foreground "Red"
    }

    if($ok_to_delete -eq "y" -or $ok_to_delete -eq "o") {
        $value.DeleteValue($key_value)
        Write-Host " | " -foreground "Green"
        Write-Host "[+] $key_value has been deleted !" -foreground "Green"
        Write-Host " | " -foreground "Green"

    } else {
        Write-Host " | " -foreground "Red"
        Write-Host "[x] Deletion aborted..." -foreground "Red"
        Write-Host " | " -foreground "Red"
    }
}

########################################################################
################################# MAIN #################################
########################################################################

function main {

    if($computer -ne "") {
        $tab_computer_name = @($computer)
        get_regedit -tab_computer $tab_computer_name -tab_key_path $tab_key_path

        if($choice -eq "delete") {
            start_remote_registry $computer
            delete_regedit $computer
            stop_remote_registry $computer
        }

    } else {
        get_regedit -tab_computer $tab_computer_name -tab_key_path $tab_key_path
    }
}

main $choice


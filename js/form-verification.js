// Description : script to control an html form
// Author : Thibaud Robin

$(document).ready(function() {

    var name = $('#name'),
        email = $('#email'),
        subject = $('#subject'),
        message = $('#message'),
        envoi = $('#envoi'),
        erreur = $('#erreur'),
        validation = $('#validation');

    var formContact = [name, email, subject, message];

    verifFormulaire(formContact, email, envoi, erreur, validation);

    function verifFormulaire(form, email, envoi, erreur, validation) {

	    for(var i = 0; i < form.length; i++)  {

	    	form[i].on('focusout submit keyup', function() {
		    	validation.css('display', 'none');

				if($(this).val() == "") {

					$(this).css(
					{ 
						borderColor : '#ebccd1',
					});
					$(this).css("background-color", "#f2dede");
					$(this).attr("placeholder", "Champ vide...");

				} else {
					$(this).css(
					{ 
						borderColor : '#d6e9c6',
					});
					$(this).css("background-color", "#ebf7e6");
					erreur.css('display', 'none');
				}
		    });
	    }


    	email.on('focusout submit keyup', function() {

	    	validation.css('display', 'none');

			if($(this).val() == "") {

				$(this).css( { 
					borderColor : '#ebccd1',
				});
				$(this).css("background-color", "#f2dede");
				$(this).attr("placeholder", "Champ vide...");

			} else {

				if(emailOK(email.val())) {
					$(this).css(
					{ 
						borderColor : '#d6e9c6',
					});
					$(this).css("background-color", "#ebf7e6");
					erreur.css('display', 'none');

				} else {
					$(this).css(
					{ 
						borderColor : '#ebccd1',
					});
					$(this).css("background-color", "#f2dede");
					$(this).attr("placeholder", "Mail incorrect...");
				}
			}
	    });

	   	
	    envoi.click(function(e) {
	    	var verif = true;

	        for(var i = 0; i < form.length; i++) {

				if(verifier(form[i])){
		    	    erreur.css('display', 'none');

		        } else {
		        	e.preventDefault();
		        	verif = false;
		        	form[i].css("borderColor", "#ebccd1");
		        	form[i].css("background-color", "#f2dede");
		        	form[i].attr("placeholder", "Champ vide...");
		        }
		    }

		    if(!verif) {
		    	alertify.error('Merci de remplir tous les champs svp...')
		    }
	    });
	}


    function verifier(champ) {

        if(champ.val() == ""){
    	    return false;
        } else {
        	return true;
        }
    }

	function emailOK(email) {

		var re =  /^[a-z][a-zA-Z0-9_.]*(\.[a-zA-Z][a-zA-Z0-9_.]*)?@[a-z][a-zA-Z-0-9]*\.[a-z]+(\.[a-z]+)?$/
		return re.test(email);
	}
});
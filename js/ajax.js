// Description : basic http query with native ajax and jquery
// Author : Thibaud Robin

// ####################### NATIVE AJAX #######################

// GET
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = () => {
	if (this.readyState == 4 && this.status == 200) {
		console.debug(this.responseText);
	}
};
xhttp.open("GET", "url", true);
xhttp.send();

// POST
// todo

// ####################### JQUERY AJAX #######################

// GET
$.ajax({
	url: "url",
	success: (result) => {
		console.debug(result);
	}
});

// POST
$.ajax({
	url: "url",
	data: {name: "Thibaud Robin"},
	method: "POST",
	success: (result) => {
		console.debug(result);
	}
});
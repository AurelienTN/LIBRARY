// Description : copy/paste this in the javascript console on linkedin to automatically invite person
// Author : Thibaud Robin
// /!\ ONLY WORK ON FIREFOX /!\

/*************************************************************************/
/******************** SEND INVITATION WITHOUT FILTRER ********************/
/*************************************************************************/
window.addEventListener('error', (e) => {
	error_msg = e.error.message

	if (error_msg.includes("429")) {
		console.error("[x] Can't send new invitation (invitation limit reached)")
		clearInterval(send_invitation);		// To stop
		clearInterval(load_new_profile);	// To stop
	}
}, true);


var load_new_profile = setInterval(() => { 
	window.scrollBy(0, 1000); 	// Scroll to appear new profiles
}, 500);


var send_invitation = setInterval(() => { 
	buttons = document.getElementsByClassName('button-secondary-small')

	if (buttons.length !== 0) {
		for (i = 0; i < buttons.length; i++) {
			buttons[i].click()
		}

		console.debug("[+] " + buttons.length + " invitations sended !")

	} else {
		console.error("[x] No new profiles available. Reload the page please")
		clearInterval(send_invitation);
		clearInterval(load_new_profile);
		document.location.refresh()
	}
}, 3000);
/*************************************************************************/


/*************************************************************************/
/********************** SEND INVITATION WITH FILTRER *********************/
/*** https://www.linkedin.com/search/results/index/?keywords=pentester ***/
/*************************************************************************/
var send_invitation = setInterval(() => { 
	buttons_invite = document.getElementsByClassName('search-result__actions--primary button-secondary-medium m5')

	for (i = 0; i < buttons_invite.length; i++) {
		buttons_invite[i].click()
		document.getElementsByClassName('button-primary-large ml1')[0].click()
	}

	console.debug("[+] " + buttons.length + " invitations sended !")

}, 3000);
/*************************************************************************/


var delete_invitation = setInterval(() => {

	try {
		document.getElementsByClassName('m0 inline-block')[0].click()
		document.getElementsByClassName('button-primary-medium')[0].click()

	} catch(error) {
		console.debug("[+] All invitations has been deleted !")
		clearInterval(delete_invitation);	// To stop
	}

}, 3000);


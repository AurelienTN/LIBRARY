#!/bin/bash

device="${BLOCK_INSTANCE:-wlp5s0}"
#device="${BLOCK_INSTANCE:-enp3s0}"
status=$(cat /sys/class/net/${device}/operstate)

URGENT_VALUE=20

if [[ "${status}" == "up" ]]; then
	if [[ -d "/sys/class/net/${device}/wireless" ]]; then
		name=$(iwgetid -r)
		quality=$(grep ${device} /proc/net/wireless | awk '{ print int($3 * 100 / 70) }')
		ip=$(ip addr show | grep wlp4s0 | grep inet | awk {'print $2'})
		echo "[${name} - ${quality}%]  $ip"

		if [[ "${quality}" -le "${URGENT_VALUE}" ]]; then
			exit 33
		fi
	else
		echo "ON"
	fi
else
	exit
fi
